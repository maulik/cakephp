jQuery(document).ready(function() {
   

    jQuery(document).on('AddMovie', function(event, id) {
        jQuery.ajax({
            type: 'POST',
            url:"movies/edit/"+id,
            success: function(data) {
                jQuery('.upd_frm').html(data);
            }
        });
    });
    
    jQuery('.add_new_movie').on('click',function(){
        var id=jQuery(this).attr('editid');
       jQuery(document).trigger('AddMovie',[id]); 
    });

});