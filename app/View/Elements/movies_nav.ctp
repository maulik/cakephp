<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//echo $this->request->params["action"];  
?>

   <!--<h2>Movies</h2>
    <ul class="nav nav-pills">
        <li <?php if($this->request->params["action"] =="index" ): ?> class="active" <?php endif; ?> ><?php echo $this->Html->link('Movies', array('controller' => 'movies', 'action' => 'index')); ?></li>
        <li  class="add_new_movie" data-toggle="modal" data-target="#myModal"><?php echo $this->Html->link('Add New', array()); ?></li>
    </ul>
   <br/>-->
  <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Cakephp Demo</a>
                </div>
                <div class="navbar-collapse collapse">
                 <ul class="nav navbar-nav">   
                 <?php if($this->UserAuth->isLogged()): ?>
                        <li class="active"><?php echo $this->Html->link('Movies', '/movies'); ?></li>
                        <li class="active"><?php echo $this->Html->link('LogOut', '/logout'); ?></li>
                 <?php else: ?>
                        <li class="active"><?php echo $this->Html->link('Login', '/login', array('class' => '', 'escape' => FALSE)); ?></li>
                        <li class="active"><?php echo $this->Html->link('Sign Up', '/register', array('class' => '', 'escape' => FALSE)); ?></li>
                 <?php endif; ?>
                 </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>