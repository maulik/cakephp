<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $cakeDescription ?>:
            <?php echo $title_for_layout; ?>
        </title>

        <?php
        echo $this->Html->meta('icon');

        //echo $this->Html->css('cake.generic');

        echo $this->Html->css(
                array(
                    'bootstrap',
                    'bootstrap-theme.min',
                    'theme'
                )
        );
        
       echo $this->Html->css('/usermgmt/css/umstyle');
//  <!-- Bootstrap core JavaScript
//    ================================================== -->
//    <!-- Placed at the end of the document so the pages load faster -->

        echo $this->Html->script(
                array(
                    'jquery',
                    'bootstrap.min',
                    'holder',
                    'comman',
        ));

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>
    </head>
    <body>
        <input type="hidden" id="site_url" value="http://localhost/cakephp238/">
        <!-- Fixed navbar -->
        
        <?php 
        echo $this->element('movies_nav');
        echo $this->Session->flash();
        ?>
        <div class="clearfix"></div>
        <?php echo $this->fetch('content'); ?>
        <?php //echo $this->element('sql_dump');    ?>
    </body>
</html>

