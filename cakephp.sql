-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 29, 2014 at 07:06 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cakephp`
--
CREATE DATABASE IF NOT EXISTS `cakephp` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `cakephp`;

-- --------------------------------------------------------

--
-- Table structure for table `login_tokens`
--

CREATE TABLE IF NOT EXISTS `login_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` char(32) NOT NULL,
  `duration` varchar(32) NOT NULL,
  `used` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `expires` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `login_tokens`
--

INSERT INTO `login_tokens` (`id`, `user_id`, `token`, `duration`, `used`, `created`, `expires`) VALUES
(1, 1, 'bec7cc8829fcd166420fc93bbfba66dd', '2 weeks', 0, '2014-03-29 00:42:40', '2014-04-12 00:42:40'),
(2, 1, '84bcb07d26ef60f989dbb68c28803792', '2 weeks', 0, '2014-03-29 00:43:15', '2014-04-12 00:43:15'),
(3, 1, '89f86b7b0a2e6513434c9f4cb7e129ae', '2 weeks', 0, '2014-03-29 01:41:14', '2014-04-12 01:41:14'),
(4, 1, '7d14938012478f1f98bf82cf67452417', '2 weeks', 0, '2014-03-29 01:42:44', '2014-04-12 01:42:44'),
(5, 1, 'b551b010f12e54031441e2ee172f7aaf', '2 weeks', 0, '2014-03-29 01:43:36', '2014-04-12 01:43:36'),
(6, 1, '1bc8d278aaf2c95cc71feb323262bbfd', '2 weeks', 0, '2014-03-29 01:48:14', '2014-04-12 01:48:14'),
(7, 1, '5cbbfe2047cc9c33e3558fdb4b9ce857', '2 weeks', 0, '2014-03-29 01:50:02', '2014-04-12 01:50:02'),
(8, 1, 'bd7dce0d9567b1d0fc444c8a8c8b4c37', '2 weeks', 0, '2014-03-29 01:50:40', '2014-04-12 01:50:40'),
(9, 1, 'c02f7fd56cf8f0bb99ab6097e8d2b6ca', '2 weeks', 0, '2014-03-29 01:52:58', '2014-04-12 01:52:58'),
(10, 1, 'a5e9c96765d11e2e83a4b14b86d502e5', '2 weeks', 0, '2014-03-29 01:53:09', '2014-04-12 01:53:09'),
(11, 1, '6627cdef79202d12f48c8ccbd97aee66', '2 weeks', 0, '2014-03-29 01:59:50', '2014-04-12 01:59:50'),
(12, 1, '9fe17c066a1e8b97fd2efc69ca4c9fe5', '2 weeks', 0, '2014-03-29 02:00:24', '2014-04-12 02:00:24'),
(13, 1, '6946cdb5f4c28ff405dbc23828557e90', '2 weeks', 0, '2014-03-29 02:01:25', '2014-04-12 02:01:25'),
(14, 2, '3675713977cd62826d23c92a6fbcd5ab', '2 weeks', 0, '2014-03-29 02:21:42', '2014-04-12 02:21:42'),
(15, 1, '8b3cdac0cf0ec858ed5f378f1451c1e6', '2 weeks', 0, '2014-03-29 02:22:08', '2014-04-12 02:22:08'),
(16, 2, '21df5246626213a014e92294338c55d2', '2 weeks', 0, '2014-03-29 02:23:03', '2014-04-12 02:23:03'),
(17, 1, 'dbbe69bf9a715388e3fbb4f332061b21', '2 weeks', 0, '2014-03-29 02:30:38', '2014-04-12 02:30:38'),
(18, 1, '79100028500bb2d9d47a8555d5c78da6', '2 weeks', 0, '2014-03-29 02:30:51', '2014-04-12 02:30:51'),
(19, 2, '8ece35ee609a85910b3188e11106e1d8', '2 weeks', 0, '2014-03-29 02:31:00', '2014-04-12 02:31:00'),
(20, 2, 'bb1d34eaebfc2e90fbc87d2958f7eec9', '2 weeks', 0, '2014-03-29 02:40:17', '2014-04-12 02:40:17'),
(21, 1, '3a50e43e7755fa95563e6176f5d0e0be', '2 weeks', 0, '2014-03-29 02:40:56', '2014-04-12 02:40:56'),
(22, 2, 'bed76d1aab743a42f92b4181fb3d7cb3', '2 weeks', 0, '2014-03-29 02:43:46', '2014-04-12 02:43:46'),
(23, 2, '584c9ecf4d099f1f657399256a8fa1d6', '2 weeks', 0, '2014-03-29 02:45:29', '2014-04-12 02:45:29'),
(24, 1, 'b871b912f515b46e8aa5f44d595c92d8', '2 weeks', 0, '2014-03-29 02:46:12', '2014-04-12 02:46:12'),
(25, 1, 'b595ec818a9474c2a8ce5573043be5d1', '2 weeks', 0, '2014-03-29 02:48:50', '2014-04-12 02:48:50');

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE IF NOT EXISTS `movies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `genre` varchar(255) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `format` varchar(255) NOT NULL,
  `length` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`id`, `title`, `genre`, `rating`, `format`, `length`) VALUES
(1, 'K3G yesdfsdfd', '2004', '1234', 'Family', 3),
(2, 'KANK', 'test', '12345', 'test', 123),
(3, 'Ramleela', 'test', 'sdflskf', 'sdlfkjsdkl', 123),
(4, 'K3G', '2004', '1234', 'Family', 3),
(5, 'K3G', '2004', '1234', 'Family', 3),
(6, 'K3G-again', '2004', '1234', 'Family', 3),
(7, 'test', 'abcd', '12345', 'asdfdf', 12345),
(8, 'test', 'abcd', '12345', 'asdfdf', 12345),
(9, 'test', 'abca', '12345', 'asdfdf', 12345),
(12, 'test', 'dfsdf', 'test', 'abc', 232323),
(13, 'test', 'abcd', 'dfd', 'dd', 0),
(15, 'newsd', 'testdf', 'klfjsdklf', 'lkjkldfkl', 0),
(16, 'test', 'test', 'test', 'test', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) unsigned DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `active` varchar(3) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`username`),
  KEY `mail` (`email`),
  KEY `users_FKIndex1` (`user_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_group_id`, `username`, `password`, `email`, `first_name`, `last_name`, `number`, `active`, `created`, `modified`) VALUES
(1, 1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'admin@admin.com', 'Admin', NULL, NULL, '1', '2014-03-29 10:04:20', '2014-03-29 10:04:20'),
(2, 2, 'test', 'e10adc3949ba59abbe56e057f20f883e', 'mk@mk.com', 'test', 'test', NULL, '1', '2014-03-29 02:20:48', '2014-03-29 02:20:48'),
(3, 2, 'maulik', 'e10adc3949ba59abbe56e057f20f883e', 'nk.mk@mk.com', 'kanani', 'kanani', '123456789', '0', '2014-03-29 02:44:32', '2014-03-29 02:44:32');

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE IF NOT EXISTS `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `alias_name` varchar(100) DEFAULT NULL,
  `allowRegistration` int(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `name`, `alias_name`, `allowRegistration`, `created`, `modified`) VALUES
(1, 'Admin', 'Admin', 0, '2014-03-29 10:04:20', '2014-03-29 10:04:20'),
(2, 'User', 'User', 1, '2014-03-29 10:04:20', '2014-03-29 10:04:20'),
(3, 'Guest', 'Guest', 0, '2014-03-29 10:04:20', '2014-03-29 10:04:20');

-- --------------------------------------------------------

--
-- Table structure for table `user_group_permissions`
--

CREATE TABLE IF NOT EXISTS `user_group_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_group_id` int(10) unsigned NOT NULL,
  `controller` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `action` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `allowed` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=82 ;

--
-- Dumping data for table `user_group_permissions`
--

INSERT INTO `user_group_permissions` (`id`, `user_group_id`, `controller`, `action`, `allowed`) VALUES
(1, 1, 'Pages', 'display', 1),
(2, 2, 'Pages', 'display', 1),
(3, 3, 'Pages', 'display', 1),
(4, 1, 'UserGroupPermissions', 'index', 1),
(5, 2, 'UserGroupPermissions', 'index', 0),
(6, 3, 'UserGroupPermissions', 'index', 0),
(7, 1, 'UserGroupPermissions', 'update', 1),
(8, 2, 'UserGroupPermissions', 'update', 0),
(9, 3, 'UserGroupPermissions', 'update', 0),
(10, 1, 'UserGroups', 'index', 1),
(11, 2, 'UserGroups', 'index', 0),
(12, 3, 'UserGroups', 'index', 0),
(13, 1, 'UserGroups', 'addGroup', 1),
(14, 2, 'UserGroups', 'addGroup', 0),
(15, 3, 'UserGroups', 'addGroup', 0),
(16, 1, 'UserGroups', 'editGroup', 1),
(17, 2, 'UserGroups', 'editGroup', 0),
(18, 3, 'UserGroups', 'editGroup', 0),
(19, 1, 'UserGroups', 'deleteGroup', 1),
(20, 2, 'UserGroups', 'deleteGroup', 0),
(21, 3, 'UserGroups', 'deleteGroup', 0),
(22, 1, 'Users', 'index', 1),
(23, 2, 'Users', 'index', 0),
(24, 3, 'Users', 'index', 0),
(25, 1, 'Users', 'viewUser', 1),
(26, 2, 'Users', 'viewUser', 0),
(27, 3, 'Users', 'viewUser', 0),
(28, 1, 'Users', 'myprofile', 1),
(29, 2, 'Users', 'myprofile', 1),
(30, 3, 'Users', 'myprofile', 0),
(31, 1, 'Users', 'login', 1),
(32, 2, 'Users', 'login', 1),
(33, 3, 'Users', 'login', 1),
(34, 1, 'Users', 'logout', 1),
(35, 2, 'Users', 'logout', 1),
(36, 3, 'Users', 'logout', 1),
(37, 1, 'Users', 'register', 1),
(38, 2, 'Users', 'register', 1),
(39, 3, 'Users', 'register', 1),
(40, 1, 'Users', 'changePassword', 1),
(41, 2, 'Users', 'changePassword', 1),
(42, 3, 'Users', 'changePassword', 0),
(43, 1, 'Users', 'changeUserPassword', 1),
(44, 2, 'Users', 'changeUserPassword', 0),
(45, 3, 'Users', 'changeUserPassword', 0),
(46, 1, 'Users', 'addUser', 1),
(47, 2, 'Users', 'addUser', 0),
(48, 3, 'Users', 'addUser', 1),
(49, 1, 'Users', 'editUser', 1),
(50, 2, 'Users', 'editUser', 0),
(51, 3, 'Users', 'editUser', 0),
(52, 1, 'Users', 'dashboard', 1),
(53, 2, 'Users', 'dashboard', 1),
(54, 3, 'Users', 'dashboard', 0),
(55, 1, 'Users', 'deleteUser', 1),
(56, 2, 'Users', 'deleteUser', 0),
(57, 3, 'Users', 'deleteUser', 0),
(58, 1, 'Users', 'makeActive', 1),
(59, 2, 'Users', 'makeActive', 0),
(60, 3, 'Users', 'makeActive', 0),
(61, 1, 'Users', 'accessDenied', 1),
(62, 2, 'Users', 'accessDenied', 1),
(63, 3, 'Users', 'accessDenied', 1),
(64, 1, 'Users', 'userVerification', 1),
(65, 2, 'Users', 'userVerification', 1),
(66, 3, 'Users', 'userVerification', 1),
(67, 1, 'Users', 'forgotPassword', 1),
(68, 2, 'Users', 'forgotPassword', 1),
(69, 3, 'Users', 'forgotPassword', 1),
(70, 1, 'Movies', 'index', 1),
(71, 2, 'Movies', 'index', 1),
(72, 3, 'Movies', 'index', 0),
(73, 1, 'Movies', 'edit', 1),
(74, 2, 'Movies', 'edit', 1),
(75, 3, 'Movies', 'edit', 0),
(76, 1, 'Movies', 'view', 1),
(77, 2, 'Movies', 'view', 1),
(78, 3, 'Movies', 'view', 0),
(79, 1, 'Movies', 'delete', 1),
(80, 2, 'Movies', 'delete', 1),
(81, 3, 'Movies', 'delete', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
